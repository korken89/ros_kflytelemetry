/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include "ros/ros.h"
#include "ros_kflytelemetry/ros_kflytelemetry_node.h"
#include "serial/serial.h"

int main(int argc, char *argv[])
{
    /*
     * Initializing ROS
     */
    ROS_INFO("Initializing KFly Telemetry...");
    ros::init(argc, argv, "ros_kflytelemetry");

    ros::NodeHandle nh("~");
    ros::NodeHandle n;

    /* Get all system parameters. */
    std::string port;
    int baud;
    int motioncapture_transfer_rate;

    if (!nh.getParam("kfly_port", port))
    {
        ROS_ERROR("No port specified, aborting!");
        return -1;
    }
    ROS_INFO("KFly port:     %s", port.c_str());

    if (!nh.getParam("kfly_baudrate", baud))
    {
        ROS_ERROR("No baudrate specified, aborting!");
        return -1;
    }
    ROS_INFO("KFly baudrate: %d", baud);

    if (baud < 9600)
    {
        ROS_ERROR("The baudrate must be 9600 or more, aborting!");
        return -1;
    }

    ROS_INFO("Listening to %s/motioncapture_pose for motion capture reference.",
             ros::this_node::getNamespace().c_str());

    if (!nh.getParam("motioncapture_send_rate", motioncapture_transfer_rate) ||
        motioncapture_transfer_rate < 0)
    {
        ROS_WARN("No motion capture send rate specified, defaulting to 5 Hz.");
        motioncapture_transfer_rate = 5;
    }
    ROS_INFO("Transferring motion capture information to KFly at %d Hz.",
            motioncapture_transfer_rate);

    ROS_INFO("Starting KFly Telemetry on port \"%s\"", port.c_str());

    /* Start it. */
    ROS_KFlyTelemetry rkt(port,
                          baud,
                          motioncapture_transfer_rate,
                          n);

    ROS_INFO("KFly connection established.");

    /* Run a multi-threaded spinner.  */
    //ros::MultiThreadedSpinner spinner(1);

    /* Let ROS run. */
    ros::spin();

    return 0;
}


/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include "ros_kflytelemetry/ros_kflytelemetry.h"

/*
 * Private methods
 */

bool ROS_KFlyTelemetry::sendPacket(KFlyTelemetryPayload::BasePayloadStruct &s,
                                   const bool ack)
{
    if (_sp->isOpen())
    {
        _sp->serialTransmit(KFlyTelemetry::KFlyTelemetry::generatePacket(s, ack));
        return true;
    }
    else
        return false;
}


void ROS_KFlyTelemetry::messageDirector(
        std::shared_ptr<KFlyTelemetryPayload::BasePayloadStruct> data)
{
    /* Check messages, first check non-status messages. */
    const ros::Time time = ros::Time::now();

    switch (data->id)
    {
        /*
         *  Priority messages that might come at a high rate.
         */

    case KFlyTelemetry::KFly_Command::GetRCValues: {

        /* Cast generic pointer to correct format. */
        auto p = static_cast< KFlyTelemetryPayload::GetRCValuesStruct* >(
                data.get()
            );

        /* Fill the payload message. */
        ros_kflytelemetry::RCValues payload;

        payload.header.stamp = time;

        payload.active_connection = static_cast<bool>(p->active_connection);
        payload.num_connections = p->num_connections;

        for (int i = 0; i < 12; i++)
            payload.channel_values[i] = p->channel_value[i];

        payload.rssi = p->rssi;
        payload.rssi_frequency = p->rssi_frequency;

        for (int i = 0; i < 12; i++)
            payload.calibrated_values[i] = p->calibrated_value[i];

        for (int i = 0; i < 3; i++)
            payload.switches[i] = static_cast<uint8_t>(p->switches[i]);

        /* Publish message. */
        _pubRCValues.publish( payload );

        break;
    }

    case KFlyTelemetry::KFly_Command::GetIMUData: {

        /* Cast generic pointer to correct format. */
        auto p = static_cast< KFlyTelemetryPayload::GetIMUDataStruct* >(
                data.get()
            );

        /* Fill the payload message. */
        ros_kflytelemetry::IMUData payload;

        payload.header.stamp = time;

        payload.acceleration.x = p->accelerometer[0];
        payload.acceleration.y = p->accelerometer[1];
        payload.acceleration.z = p->accelerometer[2];

        payload.angular_rate.x = p->gyroscope[0];
        payload.angular_rate.y = p->gyroscope[1];
        payload.angular_rate.z = p->gyroscope[2];

        payload.magnetic_field.x = p->magnetometer[0];
        payload.magnetic_field.y = p->magnetometer[1];
        payload.magnetic_field.z = p->magnetometer[2];

        payload.temperature = p->temperature;

        /* Covariance is unknown for now. */
        for (int i = 0; i < 9; i++)
        {
            payload.acceleration_covariance[i] = 0;
            payload.angular_rate_covariance[i] = 0;
            payload.magnetic_field_covariance[i] = 0;
        }

        /* Publish message. */
        _pubIMUData.publish( payload );

        break;
    }

    case KFlyTelemetry::KFly_Command::GetRawIMUData: {

        /* Cast generic pointer to correct format. */
        auto p = static_cast< KFlyTelemetryPayload::GetRawIMUDataStruct* >(
                data.get()
            );

        /* Fill the payload message. */
        ros_kflytelemetry::IMURawData payload;

        payload.header.stamp = time;

        payload.acceleration.x = p->accelerometer[0];
        payload.acceleration.y = p->accelerometer[1];
        payload.acceleration.z = p->accelerometer[2];

        payload.angular_rate.x = p->gyroscope[0];
        payload.angular_rate.y = p->gyroscope[1];
        payload.angular_rate.z = p->gyroscope[2];

        payload.magnetic_field.x = p->magnetometer[0];
        payload.magnetic_field.y = p->magnetometer[1];
        payload.magnetic_field.z = p->magnetometer[2];

        payload.temperature = p->temperature;

        payload.time_stamp_ns = p->time_stamp_ns;

        /* Covariance is unknown for now. */
        for (int i = 0; i < 9; i++)
        {
            payload.acceleration_covariance[i] = 0;
            payload.angular_rate_covariance[i] = 0;
            payload.magnetic_field_covariance[i] = 0;
        }

        /* Publish message. */
        _pubIMURawData.publish( payload );

        break;
    }

    case KFlyTelemetry::KFly_Command::GetEstimationAttitude: {

        /* Cast generic pointer to correct format. */
        auto p = static_cast< KFlyTelemetryPayload::GetEstimationAttitudeStruct* >(
                data.get()
            );

        /* Fill the payload message. */
        ros_kflytelemetry::EstimationAttitude payload;

        payload.header.stamp = time;

        payload.attitude.w = p->qw;
        payload.attitude.x = p->qx;
        payload.attitude.y = p->qy;
        payload.attitude.z = p->qz;

        payload.angular_rate.x = p->angular_rate[0];
        payload.angular_rate.y = p->angular_rate[1];
        payload.angular_rate.z = p->angular_rate[2];

        payload.rate_bias.x = p->rate_bias[0];
        payload.rate_bias.y = p->rate_bias[1];
        payload.rate_bias.z = p->rate_bias[2];

        /* Publish message. */
        _pubEstimationAttitude.publish( payload );

        break;
    }

        /*
         *  Non-critical messages.
         */

    case KFlyTelemetry::KFly_Command::ACK:
#ifndef NDEBUG
        ROS_INFO("Got ACK!");
#endif
    case KFlyTelemetry::KFly_Command::GetRunningMode:
    case KFlyTelemetry::KFly_Command::GetDeviceInfo:
    case KFlyTelemetry::KFly_Command::GetControllerLimits:
    case KFlyTelemetry::KFly_Command::GetArmSettings:
    case KFlyTelemetry::KFly_Command::GetRateControllerData:
    case KFlyTelemetry::KFly_Command::GetAttitudeControllerData:
    case KFlyTelemetry::KFly_Command::GetChannelMix:
    case KFlyTelemetry::KFly_Command::GetRCInputSettings:
    case KFlyTelemetry::KFly_Command::GetRCOutputSettings:
    case KFlyTelemetry::KFly_Command::GetIMUCalibration: {

        /* All Get messages (and ACK) should just save the pointer for the
         * internal usage. */

        std::lock_guard< std::mutex > message_locker( _srvMsgAccess );

        /* Take the pointer and signal the handler. */
        _srvMsg = data;
        _srvWaitResponse.notify_one();

        break;
    }

    default:

        ROS_WARN("Unimplemented command received.");
        break;

    }
}


/*
 * Public methods
 */

ROS_KFlyTelemetry::ROS_KFlyTelemetry(const std::string &port,
                                     const unsigned int baudrate,
                                     const unsigned int motioncapture_rate,
                                     ros::NodeHandle &nh)
    : _motionCaptueTransferRate(motioncapture_rate),
      _motionCaptureAvailable(false),
      _startExperiment(false)
{
    /* Create publishers */
    _pubIMUData =
        nh.advertise<ros_kflytelemetry::IMUData>("IMUData",
                                                 10);
    _pubIMURawData =
        nh.advertise<ros_kflytelemetry::IMURawData>("IMURawData",
                                                    10);
    _pubRCValues =
        nh.advertise<ros_kflytelemetry::RCValues>("RCValues",
                                                  10);

    _pubEstimationAttitude =
        nh.advertise<ros_kflytelemetry::EstimationAttitude>("EstimationAttitude",
                                                  10);

    /* Create subscribers */
    _subMotionCaptureMeasurement = nh.subscribe("motioncapture_pose",
                                                10,
                                                &ROS_KFlyTelemetry::subMotionCaptureMeasurement,
                                                this);

    /* Check so the motion capture is running. */
    ROS_INFO("Waiting for the motion capture system...");

    _subDirectReference = nh.subscribe("DirectReference",
                                       10,
                                       &ROS_KFlyTelemetry::subDirectReference,
                                       this);
    _subIndirectReference = nh.subscribe("IndirectReference",
                                         10,
                                         &ROS_KFlyTelemetry::subIndirectReference,
                                         this);
    _subRateReference = nh.subscribe("RateReference",
                                     10,
                                     &ROS_KFlyTelemetry::subRateReference,
                                     this);
    _subAttitudeReference = nh.subscribe("AttitudeReference",
                                         10,
                                         &ROS_KFlyTelemetry::subAttitudeReference,
                                         this);

    /* Create services */
    _srvGetRunningMode
        = nh.advertiseService("GetRunningMode",
                              &ROS_KFlyTelemetry::srvGetRunningMode,
                              this);
    _srvManageSubscription
        = nh.advertiseService("ManageSubscription",
                              &ROS_KFlyTelemetry::srvManageSubscription,
                              this);
    _srvSaveSubscription
        = nh.advertiseService("SaveSettings",
                              &ROS_KFlyTelemetry::srvSaveSettings,
                              this);
    _srvEraseSubscription
        = nh.advertiseService("EraseSettings",
                              &ROS_KFlyTelemetry::srvEraseSettings,
                              this);
    _srvGetDeviceInfo
        = nh.advertiseService("GetDeviceInfo",
                              &ROS_KFlyTelemetry::srvGetDeviceInfo,
                              this);
    _srvSetDeviceID
        = nh.advertiseService("SetDeviceID",
                              &ROS_KFlyTelemetry::srvSetDeviceID,
                              this);
    _srvGetControllerLimits
        = nh.advertiseService("GetControllerLimits",
                              &ROS_KFlyTelemetry::srvGetControllerLimits,
                              this);
    _srvSetControllerLimits
        = nh.advertiseService("SetControllerLimits",
                              &ROS_KFlyTelemetry::srvSetControllerLimits,
                              this);
    _srvGetArmSettings
        = nh.advertiseService("GetArmSettings",
                              &ROS_KFlyTelemetry::srvGetArmSettings,
                              this);
    _srvSetArmSettings
        = nh.advertiseService("SetArmSettings",
                              &ROS_KFlyTelemetry::srvSetArmSettings,
                              this);
    _srvGetRateControllerData
        = nh.advertiseService("GetRateControllerData",
                              &ROS_KFlyTelemetry::srvGetRateControllerData,
                              this);
    _srvSetRateControllerData
        = nh.advertiseService("SetRateControllerData",
                              &ROS_KFlyTelemetry::srvSetRateControllerData,
                              this);
    _srvGetAttitudeControllerData
        = nh.advertiseService("GetAttitudeControllerData",
                              &ROS_KFlyTelemetry::srvGetAttitudeControllerData,
                              this);
    _srvSetAttitudeControllerData
        = nh.advertiseService("SetAttitudeControllerData",
                              &ROS_KFlyTelemetry::srvSetAttitudeControllerData,
                              this);
    _srvGetChannelMix
        = nh.advertiseService("GetChannelMix",
                              &ROS_KFlyTelemetry::srvGetChannelMix,
                              this);
    _srvSetChannelMix
        = nh.advertiseService("SetChannelMix",
                              &ROS_KFlyTelemetry::srvSetChannelMix,
                              this);
    _srvGetRCInputSettings
        = nh.advertiseService("GetRCInputSettings",
                              &ROS_KFlyTelemetry::srvGetRCInputSettings,
                              this);
    _srvSetRCInputSettings
        = nh.advertiseService("SetRCInputSettings",
                              &ROS_KFlyTelemetry::srvSetRCInputSettings,
                              this);
    _srvGetRCOutputSettings
        = nh.advertiseService("GetRCOutputSettings",
                              &ROS_KFlyTelemetry::srvGetRCOutputSettings,
                              this);
    _srvSetRCOutputSettings
        = nh.advertiseService("SetRCOutputSettings",
                              &ROS_KFlyTelemetry::srvSetRCOutputSettings,
                              this);
    _srvGetIMUCalibration
        = nh.advertiseService("GetIMUCalibration",
                              &ROS_KFlyTelemetry::srvGetIMUCalibration,
                              this);
    _srvSetIMUCalibration
        = nh.advertiseService("SetIMUCalibration",
                              &ROS_KFlyTelemetry::srvSetIMUCalibration,
                              this);
    _srvExperiment
        = nh.advertiseService("StartExperiment",
                              &ROS_KFlyTelemetry::srvStartExperiment,
                              this);

    /* Start the serial port. */
    _sp = std::unique_ptr<SerialBridge>(
        new SerialBridge(port, baudrate, 1, false)
    );

    /* Register the message director. */
    _kt.registerCallback(
        [&] (std::shared_ptr<KFlyTelemetryPayload::BasePayloadStruct> p)
        {
            messageDirector(p);
        }
    );

    /* Allow data to start flowing by registering the parsing of serial data. */
    _sp->registerCallback(
        [&] (const std::vector<uint8_t> &data)
        {
            _kt.parse(data);
        }
    );

    /* Open the port. */
    while (ros::ok())
    {
        try {
            _sp->openPort();
        } catch (serial::IOException &e)
        {
            ROS_ERROR("Port not found, retrying in 1 second.");
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

        if (_sp->isOpen())
            break;
    }
}

ROS_KFlyTelemetry::~ROS_KFlyTelemetry()
{
    /* Manually shutdown all publishers, subscribers and services. Strange bug
     * was found when just letting all go out of scope. */

    /* Publishers and Subscribers */
    _pubIMUData.shutdown();
    _pubIMURawData.shutdown();
    _pubRCValues.shutdown();

    _subDirectReference.shutdown();
    _subIndirectReference.shutdown();
    _subRateReference.shutdown();
    _subAttitudeReference.shutdown();
    _subMotionCaptureMeasurement.shutdown();

    /* Services */
    _srvGetRunningMode.shutdown();
    _srvManageSubscription.shutdown();
    _srvGetDeviceInfo.shutdown();
    _srvSetDeviceID.shutdown();
    _srvGetControllerLimits.shutdown();
    _srvSetControllerLimits.shutdown();
    _srvGetArmSettings.shutdown();
    _srvSetArmSettings.shutdown();
    _srvGetRateControllerData.shutdown();
    _srvSetRateControllerData.shutdown();
    _srvGetAttitudeControllerData.shutdown();
    _srvSetAttitudeControllerData.shutdown();
    _srvGetChannelMix.shutdown();
    _srvSetChannelMix.shutdown();
    _srvGetRCInputSettings.shutdown();
    _srvSetRCInputSettings.shutdown();
    _srvGetRCOutputSettings.shutdown();
    _srvSetRCOutputSettings.shutdown();
    _srvGetIMUCalibration.shutdown();
    _srvSetIMUCalibration.shutdown();
}

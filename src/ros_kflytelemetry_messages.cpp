/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include "ros/ros.h"
#include "ros_kflytelemetry/ros_kflytelemetry.h"


void ROS_KFlyTelemetry::subDirectReference(
        const ros_kflytelemetry::DirectReferenceConstPtr &msg)
{
    KFlyTelemetryPayload::ComputerControlReferenceStruct s;

    /* Fill data structure */
    s.mode = KFlyTelemetryPayload::FlightMode::MOTOR_DIRECT_MODE;

    for (int i = 0; i < 8; i++)
        s.direct_control[i] = saturate(msg->throttle[i], 0.0, 1.0) * 65535;

    /* Send payload */
    sendPacket(s, false);
}


void ROS_KFlyTelemetry::subIndirectReference(
        const ros_kflytelemetry::IndirectReferenceConstPtr &msg)
{
    KFlyTelemetryPayload::ComputerControlReferenceStruct s;

    /* Fill data structure */
    s.mode = KFlyTelemetryPayload::FlightMode::MOTOR_INDIRECT_MODE;

    s.indirect_control.roll = saturate(msg->torque.x, -1.0, 1.0);
    s.indirect_control.pitch = saturate(msg->torque.y, -1.0, 1.0);
    s.indirect_control.yaw = saturate(msg->torque.z, -1.0, 1.0);
    s.indirect_control.throttle = saturate(msg->throttle, 0.0, 1.0);

    /* Send payload */
    sendPacket(s, false);
}


void ROS_KFlyTelemetry::subRateReference(
        const ros_kflytelemetry::RateReferenceConstPtr &msg)
{
    KFlyTelemetryPayload::ComputerControlReferenceStruct s;

    /* Fill data structure */
    s.mode = KFlyTelemetryPayload::FlightMode::RATE_MODE;

    s.rate.roll = msg->rate.x;
    s.rate.pitch = msg->rate.y;
    s.rate.yaw = msg->rate.z;
    s.rate.throttle = saturate(msg->throttle, 0.0, 1.0);

    /* Send payload */
    sendPacket(s, false);
}


void ROS_KFlyTelemetry::subAttitudeReference(
        const ros_kflytelemetry::AttitudeReferenceConstPtr &msg)
{
    KFlyTelemetryPayload::ComputerControlReferenceStruct s;

    /* Fill data structure */
    s.mode = KFlyTelemetryPayload::FlightMode::ATTITUDE_MODE;

    s.attitude.w = msg->attitude.w;
    s.attitude.x = msg->attitude.x;
    s.attitude.y = msg->attitude.y;
    s.attitude.z = msg->attitude.z;
    s.attitude.throttle = saturate(msg->throttle, 0.0, 1.0);

    /* Send payload */
    sendPacket(s, false);
}

void ROS_KFlyTelemetry::subMotionCaptureMeasurement(
        const geometry_msgs::TransformStampedConstPtr &msg)
{
    /* Delta time from the transfer rate. */
    const double mc_dt = 1.0 / (double)_motionCaptueTransferRate;

    /* Initialize the starting time with the current time. */
    static ros::Time t = ros::Time::now();

    /* Set the motion capture availability. */
    if (!_motionCaptureAvailable)
    {
        _motionCaptureAvailable = true;
        ROS_INFO("Motion capture online, forwarding data to KFly.");
    }

    /* Calculate the time between the current sample and
     * the last transfered one. */
    ros::Duration d = msg->header.stamp - t;

    if (d.toSec() >= mc_dt)
    {
        /* Set the timestamp to wait for the next value. */
        t = msg->header.stamp;

        KFlyTelemetryPayload::MotionCaptureFrameStruct frame;

        /* Add the frame. */
        frame.framenumber = msg->header.seq;

        frame.x = msg->transform.translation.x;
        frame.y = msg->transform.translation.y;
        frame.z = msg->transform.translation.z;

        frame.qw = msg->transform.rotation.w;
        frame.qx = msg->transform.rotation.x;
        frame.qy = msg->transform.rotation.y;
        frame.qz = msg->transform.rotation.z;

        /* Transmit! (no ack) */
        sendPacket(frame, false);
    }
}

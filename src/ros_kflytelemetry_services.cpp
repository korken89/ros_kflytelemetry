/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include "ros/ros.h"
#include "ros_kflytelemetry/ros_kflytelemetry.h"

bool ROS_KFlyTelemetry::srvWaitForResponse(
        std::unique_lock< std::mutex > &lock,
        const unsigned int wait)
{
    _srvWaitResponse.wait_for(
        lock, std::chrono::milliseconds(wait), [&](){

        if (_srvMsg)
            return true;
        else
            return false;
    });

    if (_srvMsg)
        return true;
    else
        return false;
}


bool ROS_KFlyTelemetry::sendSetCommand(KFlyTelemetryPayload::BasePayloadStruct &s,
                                       std::unique_lock< std::mutex > &msg_lock)
{
    /* Send packet and wait for ACK. */
    if (!sendPacket(s, true))
        return false;

    /* Wait for the information. */
    if (srvWaitForResponse(msg_lock, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::ACK))
        return true;
    else
        return false;
}


bool ROS_KFlyTelemetry::srvGetRunningMode(
        ros_kflytelemetry::GetRunningMode::Request &req,
        ros_kflytelemetry::GetRunningMode::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetRunningMode.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetRunningMode;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetRunningMode))
    {
        /* Answer the request. */
        auto p = static_cast< const KFlyTelemetryPayload::GetRunningModeStruct* >(
                _srvMsg.get());
        res.success = true;
        if (p->sel == 'P')
            res.mode = "program";
        else
            res.mode = "bootloader";

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvManageSubscription(
        ros_kflytelemetry::ManageSubscription::Request &req,
        ros_kflytelemetry::ManageSubscription::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for ManageSubscription.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Send the subscription. */
    KFlyTelemetryPayload::ManageSubscriptionStruct s(
            static_cast<KFlyTelemetryPayload::Ports>(req.port),
            static_cast<KFlyTelemetry::KFly_Command>(req.cmd),
                                                     req.subscribe,
                                                     req.delta_ms);

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvSaveSettings(
        ros_kflytelemetry::GenericNoData::Request &req,
        ros_kflytelemetry::GenericNoData::Response &res)
{
    (void) req;

    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SaveSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Send the subscription. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::SaveToFlash;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvEraseSettings(
        ros_kflytelemetry::EraseSettings::Request &req,
        ros_kflytelemetry::EraseSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SaveSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Send the subscription. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::EraseFlash;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvGetDeviceInfo(
        ros_kflytelemetry::GetDeviceInfo::Request &req,
        ros_kflytelemetry::GetDeviceInfo::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetDeviceInfo.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetDeviceInfo;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetDeviceInfo))
    {
        auto p = static_cast< const KFlyTelemetryPayload::GetDeviceInfoStruct* >(
                _srvMsg.get());
        res.success = true;

        /* Answer the request. */
        for (int i = 0; i < 12; i++)
            res.unique_id[i] = p->unique_id[i];

        res.bootloader_version = p->bootloader_version;
        res.firmware_version = p->firmware_version;
        res.user_string = p->user_string;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetDeviceID(
        ros_kflytelemetry::SetDeviceID::Request &req,
        ros_kflytelemetry::SetDeviceID::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetDeviceID.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::SetDeviceIDStruct s(req.user_string);

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetControllerLimits(
        ros_kflytelemetry::GetControllerLimits::Request &req,
        ros_kflytelemetry::GetControllerLimits::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetControllerLimits.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetControllerLimits;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetControllerLimits))
    {
        auto p = static_cast< const KFlyTelemetryPayload::ControllerLimitsStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.max_rate_roll = p->max_rate.roll;
        res.max_rate_pitch = p->max_rate.pitch;
        res.max_rate_yaw = p->max_rate.yaw;

        res.max_rate_attitude_roll = p->max_rate_attitude.roll;
        res.max_rate_attitude_pitch = p->max_rate_attitude.pitch;
        res.max_rate_attitude_yaw = p->max_rate_attitude.yaw;

        res.max_angle_roll = p->max_angle.roll;
        res.max_angle_pitch = p->max_angle.pitch;

        res.max_velocity_horizontal = p->max_velocity.horizontal;
        res.max_velocity_vertical = p->max_velocity.vertical;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetControllerLimits(
        ros_kflytelemetry::SetControllerLimits::Request &req,
        ros_kflytelemetry::SetControllerLimits::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetControllerLimits.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::ControllerLimitsStruct s;

    s.max_rate.roll = req.max_rate_roll;
    s.max_rate.pitch = req.max_rate_pitch;
    s.max_rate.yaw = req.max_rate_yaw;

    s.max_rate_attitude.roll = req.max_rate_attitude_roll;
    s.max_rate_attitude.pitch = req.max_rate_attitude_pitch;
    s.max_rate_attitude.yaw = req.max_rate_attitude_yaw;

    s.max_angle.roll = req.max_angle_roll;
    s.max_angle.pitch = req.max_angle_pitch;

    s.max_velocity.horizontal = req.max_velocity_horizontal;
    s.max_velocity.vertical = req.max_velocity_vertical;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetArmSettings(
        ros_kflytelemetry::GetArmSettings::Request &req,
        ros_kflytelemetry::GetArmSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetArmSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetArmSettings;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetArmSettings))
    {
        auto p = static_cast< const KFlyTelemetryPayload::ArmSettingsStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.stick_threshold = p->stick_threshold;
        res.armed_min_throttle = p->armed_min_throttle;
        res.stick_direction = static_cast<uint8_t>(p->stick_direction);
        res.arm_stick_time = p->arm_stick_time;
        res.arm_zero_throttle_timeout = p->arm_zero_throttle_timeout;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetArmSettings(
        ros_kflytelemetry::SetArmSettings::Request &req,
        ros_kflytelemetry::SetArmSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetArmSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::ArmSettingsStruct s;

    s.stick_threshold = saturate(req.stick_threshold, 0.0, 1.0);
    s.armed_min_throttle = saturate(req.armed_min_throttle, 0.0, 1.0);
    s.stick_direction = static_cast<
        KFlyTelemetryPayload::Arming_Stick_Direction>(req.stick_direction);
    s.arm_stick_time = req.arm_stick_time;
    s.arm_zero_throttle_timeout = req.arm_zero_throttle_timeout;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetRateControllerData(
        ros_kflytelemetry::GetRateControllerData::Request &req,
        ros_kflytelemetry::GetRateControllerData::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetRateControllerData.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetRateControllerData;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetRateControllerData))
    {
        auto p = static_cast< const KFlyTelemetryPayload::ControllerDataStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.roll_P = p->roll_controller.P_gain;
        res.roll_I = p->roll_controller.I_gain;

        res.pitch_P = p->pitch_controller.P_gain;
        res.pitch_I = p->pitch_controller.I_gain;

        res.yaw_P = p->yaw_controller.P_gain;
        res.yaw_I = p->yaw_controller.I_gain;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetRateControllerData(
        ros_kflytelemetry::SetRateControllerData::Request &req,
        ros_kflytelemetry::SetRateControllerData::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetRateControllerData.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::ControllerDataStruct
        s(KFlyTelemetry::KFly_Command::SetRateControllerData);

    s.roll_controller.P_gain = req.roll_P;
    s.roll_controller.I_gain = req.roll_I;

    s.pitch_controller.P_gain = req.pitch_P;
    s.pitch_controller.I_gain = req.pitch_I;

    s.yaw_controller.P_gain = req.yaw_P;
    s.yaw_controller.I_gain = req.yaw_I;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetAttitudeControllerData(
        ros_kflytelemetry::GetAttitudeControllerData::Request &req,
        ros_kflytelemetry::GetAttitudeControllerData::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetAttitudeControllerData.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetAttitudeControllerData;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetAttitudeControllerData))
    {
        auto p = static_cast< const KFlyTelemetryPayload::ControllerDataStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.roll_P = p->roll_controller.P_gain;
        res.roll_I = p->roll_controller.I_gain;

        res.pitch_P = p->pitch_controller.P_gain;
        res.pitch_I = p->pitch_controller.I_gain;

        res.yaw_P = p->yaw_controller.P_gain;
        res.yaw_I = p->yaw_controller.I_gain;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetAttitudeControllerData(
        ros_kflytelemetry::SetAttitudeControllerData::Request &req,
        ros_kflytelemetry::SetAttitudeControllerData::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetAttitudeControllerData.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::ControllerDataStruct
        s(KFlyTelemetry::KFly_Command::SetAttitudeControllerData);

    s.roll_controller.P_gain = req.roll_P;
    s.roll_controller.I_gain = req.roll_I;

    s.pitch_controller.P_gain = req.pitch_P;
    s.pitch_controller.I_gain = req.pitch_I;

    s.yaw_controller.P_gain = req.yaw_P;
    s.yaw_controller.I_gain = req.yaw_I;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetChannelMix(
        ros_kflytelemetry::GetChannelMix::Request &req,
        ros_kflytelemetry::GetChannelMix::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetChannelMix.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetChannelMix;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetChannelMix))
    {
        auto p = static_cast< const KFlyTelemetryPayload::ChannelMixStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 4; j++)
                res.weights[i*4 + j] = p->weights[i][j];

            res.offset[i] = p->offset[i];
        }

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetChannelMix(
        ros_kflytelemetry::SetChannelMix::Request &req,
        ros_kflytelemetry::SetChannelMix::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetChannelMix.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::ChannelMixStruct s;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 4; j++)
            s.weights[i][j] = saturate(req.weights[i*4 + j], -1.0, 1.0);

        s.offset[i] = saturate(req.offset[i], -1.0, 1.0);
    }

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}


bool ROS_KFlyTelemetry::srvGetRCInputSettings(
        ros_kflytelemetry::GetRCInputSettings::Request &req,
        ros_kflytelemetry::GetRCInputSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetRCInputSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetRCInputSettings;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetRCInputSettings))
    {
        auto p = static_cast< const KFlyTelemetryPayload::RCInputSettingsStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.input_mode = static_cast<uint8_t>( p->mode );
        res.use_rssi = static_cast<bool>( p->use_rssi );

        for (int i = 0; i < 12; i++)
        {
            res.input_role[i] = static_cast<uint8_t>( p->role[i] );
            res.input_type[i] = static_cast<uint8_t>( p->type[i] );
            res.channel_reverse[i] = p->ch_reverse[i];
            res.channel_top[i] = p->ch_top[i];
            res.channel_center[i] = p->ch_center[i];
            res.channel_bottom[i] = p->ch_bottom[i];
        }

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetRCInputSettings(
        ros_kflytelemetry::SetRCInputSettings::Request &req,
        ros_kflytelemetry::SetRCInputSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetRCInputSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::RCInputSettingsStruct s;

    s.mode = static_cast<KFlyTelemetryPayload::RCInput_Mode>( req.input_mode );
    s.use_rssi = static_cast<uint16_t>( req.use_rssi );

    for (int i = 0; i < 12; i++)
    {
        s.role[i] =
            static_cast<KFlyTelemetryPayload::RCInput_Role>( req.input_role[i] );
        s.type[i] =
            static_cast<KFlyTelemetryPayload::RCInput_Type>( req.input_type[i] );
        s.ch_reverse[i] = req.channel_reverse[i];
        s.ch_top[i] = req.channel_top[i];
        s.ch_center[i] = req.channel_center[i];
        s.ch_bottom[i] = req.channel_bottom[i];
    }

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvGetRCOutputSettings(
        ros_kflytelemetry::GetRCOutputSettings::Request &req,
        ros_kflytelemetry::GetRCOutputSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetRCOutputSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetRCOutputSettings;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetRCOutputSettings))
    {
        auto p = static_cast< const KFlyTelemetryPayload::RCOutputSettingsStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.mode_bank1 = static_cast<uint8_t>( p->mode_bank1 );
        res.mode_bank2 = static_cast<uint8_t>( p->mode_bank2 );

        for (int i = 0; i < 8; i++)
        {
            res.channel_enabled[i] = p->channel_enabled[i];
        }

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetRCOutputSettings(
        ros_kflytelemetry::SetRCOutputSettings::Request &req,
        ros_kflytelemetry::SetRCOutputSettings::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetRCOutputSettings.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::RCOutputSettingsStruct s;

    s.mode_bank1 = static_cast<KFlyTelemetryPayload::RCOutput_Mode>( req.mode_bank1 );
    s.mode_bank2 = static_cast<KFlyTelemetryPayload::RCOutput_Mode>( req.mode_bank2 );

    for (int i = 0; i < 8; i++)
    {
        s.channel_enabled[i] = req.channel_enabled[i];
    }

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvGetIMUCalibration(
        ros_kflytelemetry::GetIMUCalibration::Request &req,
        ros_kflytelemetry::GetIMUCalibration::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for GetIMUCalibration.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Request the information. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::GetIMUCalibration;

    if (!sendPacket(s, false))
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }

    /* Wait for the information. */
    if (srvWaitForResponse(message_locker, 500) &&
        (_srvMsg->id == KFlyTelemetry::KFly_Command::GetIMUCalibration))
    {
        auto p = static_cast< KFlyTelemetryPayload::IMUCalibrationStruct* >(
                _srvMsg.get());

        /* Answer the request. */
        res.success = true;

        res.accelerometer_bias.x = p->accelerometer_bias[0];
        res.accelerometer_bias.y = p->accelerometer_bias[1];
        res.accelerometer_bias.z = p->accelerometer_bias[2];

        res.accelerometer_gain.x = p->accelerometer_gain[0];
        res.accelerometer_gain.y = p->accelerometer_gain[1];
        res.accelerometer_gain.z = p->accelerometer_gain[2];

        res.magnetometer_bias.x = p->magnetometer_bias[0];
        res.magnetometer_bias.y = p->magnetometer_bias[1];
        res.magnetometer_bias.z = p->magnetometer_bias[2];

        res.magnetometer_gain.x = p->magnetometer_gain[0];
        res.magnetometer_gain.y = p->magnetometer_gain[1];
        res.magnetometer_gain.z = p->magnetometer_gain[2];

        ros::Time time(p->timestamp, 0); // Seconds, nanoseconds
        res.timestamp.data = time;

        return true;
    }
    else
    {
        res.success = false;

        /* Must return true to not get an error in the service call. */
        return true;
    }
}


bool ROS_KFlyTelemetry::srvSetIMUCalibration(
        ros_kflytelemetry::SetIMUCalibration::Request &req,
        ros_kflytelemetry::SetIMUCalibration::Response &res)
{
    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for SetIMUCalibration.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Set the payload. */
    KFlyTelemetryPayload::IMUCalibrationStruct s;

    s.accelerometer_bias[0] = req.accelerometer_bias.x;
    s.accelerometer_bias[1] = req.accelerometer_bias.y;
    s.accelerometer_bias[2] = req.accelerometer_bias.z;

    s.accelerometer_gain[0] = req.accelerometer_gain.x;
    s.accelerometer_gain[1] = req.accelerometer_gain.y;
    s.accelerometer_gain[2] = req.accelerometer_gain.z;

    s.magnetometer_bias[0] = req.magnetometer_bias.x;
    s.magnetometer_bias[1] = req.magnetometer_bias.y;
    s.magnetometer_bias[2] = req.magnetometer_bias.z;

    s.magnetometer_gain[0] = req.magnetometer_gain.x;
    s.magnetometer_gain[1] = req.magnetometer_gain.y;
    s.magnetometer_gain[2] = req.magnetometer_gain.z;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    /* Must return true to not get an error in the service call. */
    return true;
}

bool ROS_KFlyTelemetry::srvStartExperiment(
        ros_kflytelemetry::GenericNoData::Request &req,
        ros_kflytelemetry::GenericNoData::Response &res)
{
    (void) req;

    std::lock_guard< std::mutex > service_locker( _srvAccess );
    std::unique_lock< std::mutex > message_locker( _srvMsgAccess );

    /* Log for ROS */
    ROS_INFO("Got request for StartExperiment.");

    /* Setup the return pointer. */
    _srvMsg.reset();

    /* Send the subscription. */
    KFlyTelemetryPayload::BasePayloadStruct s;
    s.id = KFlyTelemetry::KFly_Command::Experiment;

    bool result = sendSetCommand(s, message_locker);

    res.success = result;

    if (result)
        _startExperiment = true;

    /* Must return true to not get an error in the service call. */
    return true;
}

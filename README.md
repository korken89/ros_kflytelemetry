# A ROS package for KFly Telemetry

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

---

## Instructions / Guides / Information

Is available in the wiki, it has:
* Description
    * Hardware description
    * Ports description
    * Small software overview
* Installation & Configuration
* How to use the node


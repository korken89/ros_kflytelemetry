/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#ifndef _ROS_KFLYTELEMETRY_H
#define _ROS_KFLYTELEMETRY_H

/* System includes */
#include <functional>
#include <thread>
#include <string>
#include <iostream>
#include <fstream>

/* Library includes */
#include "ros/ros.h"
#include "KFlyTelemetry/kfly_telemetry.h"
#include "SerialPipe/serialpipe.h"
#include "KFlyTelemetry/kfly_telemetry.h"

/* Service includes */
#include "ros_kflytelemetry/GetRunningMode.h"
#include "ros_kflytelemetry/ManageSubscription.h"
#include "ros_kflytelemetry/GenericNoData.h"
#include "ros_kflytelemetry/EraseSettings.h"
#include "ros_kflytelemetry/GetDeviceInfo.h"
#include "ros_kflytelemetry/SetDeviceID.h"
#include "ros_kflytelemetry/GetControllerLimits.h"
#include "ros_kflytelemetry/SetControllerLimits.h"
#include "ros_kflytelemetry/GetArmSettings.h"
#include "ros_kflytelemetry/SetArmSettings.h"
#include "ros_kflytelemetry/GetRateControllerData.h"
#include "ros_kflytelemetry/SetRateControllerData.h"
#include "ros_kflytelemetry/GetAttitudeControllerData.h"
#include "ros_kflytelemetry/SetAttitudeControllerData.h"
#include "ros_kflytelemetry/GetChannelMix.h"
#include "ros_kflytelemetry/SetChannelMix.h"
#include "ros_kflytelemetry/GetRCInputSettings.h"
#include "ros_kflytelemetry/SetRCInputSettings.h"
#include "ros_kflytelemetry/GetRCOutputSettings.h"
#include "ros_kflytelemetry/SetRCOutputSettings.h"
#include "ros_kflytelemetry/GetIMUCalibration.h"
#include "ros_kflytelemetry/SetIMUCalibration.h"

/* Message includes */
#include "ros_kflytelemetry/RCValues.h"
#include "ros_kflytelemetry/IMUData.h"
#include "ros_kflytelemetry/IMURawData.h"
#include "ros_kflytelemetry/DirectReference.h"
#include "ros_kflytelemetry/IndirectReference.h"
#include "ros_kflytelemetry/RateReference.h"
#include "ros_kflytelemetry/AttitudeReference.h"
#include "ros_kflytelemetry/EstimationAttitude.h"
#include "geometry_msgs/TransformStamped.h"

using namespace SerialPipe;

class ROS_KFlyTelemetry
{
private:

    /* Auxillary functions */
    template<typename T>
    inline T saturate(const T val, const T min, const T max) {
        return std::min(std::max(val, min), max);
    }

    /* Services */
    bool srvWaitForResponse(
            std::unique_lock< std::mutex > &lock,
            const unsigned int wait);

    /* Compound function for sending a "Set"-command. */
    bool sendSetCommand(KFlyTelemetryPayload::BasePayloadStruct &s,
                        std::unique_lock< std::mutex > &msg_lock);

    bool srvGetRunningMode(
            ros_kflytelemetry::GetRunningMode::Request &req,
            ros_kflytelemetry::GetRunningMode::Response &res);

    bool srvManageSubscription(
            ros_kflytelemetry::ManageSubscription::Request &req,
            ros_kflytelemetry::ManageSubscription::Response &res);

    bool srvSaveSettings(
            ros_kflytelemetry::GenericNoData::Request &req,
            ros_kflytelemetry::GenericNoData::Response &res);

    bool srvEraseSettings(
            ros_kflytelemetry::EraseSettings::Request &req,
            ros_kflytelemetry::EraseSettings::Response &res);

    bool srvGetDeviceInfo(
            ros_kflytelemetry::GetDeviceInfo::Request &req,
            ros_kflytelemetry::GetDeviceInfo::Response &res);

    bool srvSetDeviceID(
            ros_kflytelemetry::SetDeviceID::Request &req,
            ros_kflytelemetry::SetDeviceID::Response &res);

    bool srvGetControllerLimits(
            ros_kflytelemetry::GetControllerLimits::Request &req,
            ros_kflytelemetry::GetControllerLimits::Response &res);

    bool srvSetControllerLimits(
            ros_kflytelemetry::SetControllerLimits::Request &req,
            ros_kflytelemetry::SetControllerLimits::Response &res);

    bool srvGetArmSettings(
            ros_kflytelemetry::GetArmSettings::Request &req,
            ros_kflytelemetry::GetArmSettings::Response &res);

    bool srvSetArmSettings(
            ros_kflytelemetry::SetArmSettings::Request &req,
            ros_kflytelemetry::SetArmSettings::Response &res);

    bool srvGetRateControllerData(
            ros_kflytelemetry::GetRateControllerData::Request &req,
            ros_kflytelemetry::GetRateControllerData::Response &res);

    bool srvSetRateControllerData(
            ros_kflytelemetry::SetRateControllerData::Request &req,
            ros_kflytelemetry::SetRateControllerData::Response &res);

    bool srvGetAttitudeControllerData(
            ros_kflytelemetry::GetAttitudeControllerData::Request &req,
            ros_kflytelemetry::GetAttitudeControllerData::Response &res);

    bool srvSetAttitudeControllerData(
            ros_kflytelemetry::SetAttitudeControllerData::Request &req,
            ros_kflytelemetry::SetAttitudeControllerData::Response &res);

    bool srvGetChannelMix(
            ros_kflytelemetry::GetChannelMix::Request &req,
            ros_kflytelemetry::GetChannelMix::Response &res);

    bool srvSetChannelMix(
            ros_kflytelemetry::SetChannelMix::Request &req,
            ros_kflytelemetry::SetChannelMix::Response &res);

    bool srvGetRCInputSettings(
            ros_kflytelemetry::GetRCInputSettings::Request &req,
            ros_kflytelemetry::GetRCInputSettings::Response &res);

    bool srvSetRCInputSettings(
            ros_kflytelemetry::SetRCInputSettings::Request &req,
            ros_kflytelemetry::SetRCInputSettings::Response &res);

    bool srvGetRCOutputSettings(
            ros_kflytelemetry::GetRCOutputSettings::Request &req,
            ros_kflytelemetry::GetRCOutputSettings::Response &res);

    bool srvSetRCOutputSettings(
            ros_kflytelemetry::SetRCOutputSettings::Request &req,
            ros_kflytelemetry::SetRCOutputSettings::Response &res);

    bool srvGetIMUCalibration(
            ros_kflytelemetry::GetIMUCalibration::Request &req,
            ros_kflytelemetry::GetIMUCalibration::Response &res);

    bool srvSetIMUCalibration(
            ros_kflytelemetry::SetIMUCalibration::Request &req,
            ros_kflytelemetry::SetIMUCalibration::Response &res);

    bool srvStartExperiment(
            ros_kflytelemetry::GenericNoData::Request &req,
            ros_kflytelemetry::GenericNoData::Response &res);

    /* Subscriber callbacks */

    void subDirectReference(
            const ros_kflytelemetry::DirectReferenceConstPtr &msg);

    void subIndirectReference(
            const ros_kflytelemetry::IndirectReferenceConstPtr &msg);

    void subRateReference(
            const ros_kflytelemetry::RateReferenceConstPtr &msg);

    void subAttitudeReference(
            const ros_kflytelemetry::AttitudeReferenceConstPtr &msg);

    void subMotionCaptureMeasurement(
            const geometry_msgs::TransformStampedConstPtr &msg);


    /* Send packet */
    bool sendPacket(KFlyTelemetryPayload::BasePayloadStruct &s, const bool ack);


    /* Message director */
    void messageDirector(
        std::shared_ptr<KFlyTelemetryPayload::BasePayloadStruct> data);

    /* Variables */
    std::unique_ptr<SerialBridge> _sp;
    KFlyTelemetry::KFlyTelemetry _kt;

    /** @biref Used for communication between messageDirecor and the service
     *         waiting for a response. */
    std::shared_ptr<KFlyTelemetryPayload::BasePayloadStruct> _srvMsg;

    /** @brief Access to the services (only one may run at a time). */
    std::mutex _srvAccess;

    /** @brief Access to the @p _srvMsg object. */
    std::mutex _srvMsgAccess;

    /** @brief Used to sync and timeout the response from messageDirector for
     *         @p _srvMsg. */
    std::condition_variable _srvWaitResponse;

    /** @brief Transfer rate for the motion capture frames to KFly. */
    const unsigned int _motionCaptueTransferRate;

    /** @brief Set when a motion capture frame is detected the first time. */
    bool _motionCaptureAvailable;

    /** @brief Temporary variables for experiments. */
    bool _startExperiment;
    int _experimentCount;
    std::ofstream _expSave;

    /* Publishers and Subscribers */
    ros::Publisher _pubIMUData;
    ros::Publisher _pubIMURawData;
    ros::Publisher _pubRCValues;
    ros::Publisher _pubEstimationAttitude;

    ros::Subscriber _subDirectReference;
    ros::Subscriber _subIndirectReference;
    ros::Subscriber _subRateReference;
    ros::Subscriber _subAttitudeReference;
    ros::Subscriber _subMotionCaptureMeasurement;

    /* Services */
    ros::ServiceServer _srvGetRunningMode;
    ros::ServiceServer _srvManageSubscription;
    ros::ServiceServer _srvSaveSubscription;
    ros::ServiceServer _srvEraseSubscription;
    ros::ServiceServer _srvGetDeviceInfo;
    ros::ServiceServer _srvSetDeviceID;
    ros::ServiceServer _srvGetControllerLimits;
    ros::ServiceServer _srvSetControllerLimits;
    ros::ServiceServer _srvGetArmSettings;
    ros::ServiceServer _srvSetArmSettings;
    ros::ServiceServer _srvGetRateControllerData;
    ros::ServiceServer _srvSetRateControllerData;
    ros::ServiceServer _srvGetAttitudeControllerData;
    ros::ServiceServer _srvSetAttitudeControllerData;
    ros::ServiceServer _srvGetChannelMix;
    ros::ServiceServer _srvSetChannelMix;
    ros::ServiceServer _srvGetRCInputSettings;
    ros::ServiceServer _srvSetRCInputSettings;
    ros::ServiceServer _srvGetRCOutputSettings;
    ros::ServiceServer _srvSetRCOutputSettings;
    ros::ServiceServer _srvGetIMUCalibration;
    ros::ServiceServer _srvSetIMUCalibration;
    ros::ServiceServer _srvExperiment;

public:
    ROS_KFlyTelemetry(const std::string &port,
                      const unsigned int baudrate,
                      const unsigned int motioncapture_rate,
                      ros::NodeHandle &nh);

    ~ROS_KFlyTelemetry();
};

#endif
